package main

// write a function that takes a string and passes it
// as an argument to an executable program in the
// same directory. The program should return the
// string
import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"github.com/gin-gonic/gin"
)

func callProgram(s string) string {
	cmd := exec.Command("./Eliza", s)
	out, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
		return "ERROR"
	}
	return string(out)
}

// ConfigRuntime sets the number of operating system threads.
func ConfigRuntime() {
	fmt.Println("Configuring runtime...")
	nuCPU := runtime.NumCPU()
	runtime.GOMAXPROCS(nuCPU)
	fmt.Printf("Running with %d CPUs\n", nuCPU)
}

// StartGin starts gin web server with setting router.
func StartGin() {
	gin.SetMode(gin.ReleaseMode)

	fmt.Println("Starting gin web server...")
	router := gin.New()

	// GET request to /paip/eliza/:encodedInput
	// encodedInput is a base64 encoded string of the statement
	// to be answered by ELIZA
	//
	// Returns a JSON object with the following fields:
	// 		- statement:     the sentence given by the user
	router.GET("/paip/eliza/:encodedInput", func(c *gin.Context) {
		encodedInput := c.Param("encodedInput")
		decodedInput, err := base64.StdEncoding.DecodeString(encodedInput)
		if err != nil {
			log.Panicf("error: %s", err)
		}

		statement := string(decodedInput)
		response := callProgram(statement)
		c.IndentedJSON(http.StatusOK, gin.H{
			"response": response,
		})
	})

	// Listen on port 8080
	fmt.Println("Listening on port 8080...")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := router.Run(":" + port); err != nil {
        log.Panicf("error: %s", err)
	}
}

func main() {
	ConfigRuntime()
	StartGin()
}